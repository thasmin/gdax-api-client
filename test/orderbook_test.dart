import 'dart:async';
import 'package:test/test.dart';

import 'package:gdaxclient/gdax_orderbook.dart';

bool got = false;
void onTickerChange(event){
  got = true;
  print(event);
}

void onUpdate() {
}

void main() {
  var orderbook = new OrderBook('BTC-USD', onTickerChange, onUpdate);

  new Future.delayed(const Duration(seconds: 1), () {
    expect(got, true);
    orderbook.close();
  });
}
