import 'package:flutter/material.dart';
import 'package:gdaxclient/BottomNav.dart';
import 'package:gdaxclient/gdax_api.dart';


class OrderPageRoute extends MaterialPageRoute {
  OrderPageRoute(GdaxOrder order) : super(builder: (ctx) => new OrderView(order));
}

class DataTableRow extends TableRow {
  DataTableRow(ThemeData theme, String heading, String value) : super(
    children: [
      new Padding( padding: const EdgeInsets.all(4.0), child: new Text(heading, style: new TextStyle(color: theme.primaryColor))),
      new Padding( padding: const EdgeInsets.all(4.0), child: new Text(value)),
    ]
  );
}

class OrderView extends StatelessWidget {
  final GdaxOrder order;
  OrderView(this.order);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return new Scaffold(
      appBar: new AppBar(
      title: new Text('Order from ' + order.createdAt.toString())),
      body: new Padding(
        padding: const EdgeInsets.all(8.0),
        child: new Padding(
          padding: const EdgeInsets.all(8.0),
            child: new Table(
              columnWidths: { 0: const IntrinsicColumnWidth() },
              children: [
                new DataTableRow(theme, 'Product', order.productId),
                new DataTableRow(theme, 'Side', order.side),
                new DataTableRow(theme, 'STP', order.stp),
                new DataTableRow(theme, 'Funds', order.hasFunds ? order.funds.toString() : ''),
                new DataTableRow(theme, 'Specified Funds', order.hasFunds ? order.specifiedFunds.toString() : ''),
                new DataTableRow(theme, 'Type', order.type),
                new DataTableRow(theme, 'Post Only', order.postOnly.toString()),
                new DataTableRow(theme, 'Created At', order.createdAt.toString()),
                new DataTableRow(theme, 'Done At', order.doneAt.toString()),
                new DataTableRow(theme, 'Done At Reason', order.doneAtReason ?? ''),
                new DataTableRow(theme, 'Fill Fees', order.fillFees.toString()),
                new DataTableRow(theme, 'Filled Size', order.filledSize.toString()),
                new DataTableRow(theme, 'Executed Value', order.executedValue.toString()),
                new DataTableRow(theme, 'Status', order.status),
                new DataTableRow(theme, 'Settled', order.settled ? 'Yes' : 'No'),
              ],
            )
        )
      )
    );
  }
}

class OrdersView extends StatefulWidget {
  OrdersView({Key key}) : super(key: key);
  @override
  _OrdersViewState createState() => new _OrdersViewState();
}

class _OrdersViewState extends State<OrdersView> {
  List<GdaxOrder> _orders = new List<GdaxOrder>();

  @override
  void initState() {
    new GdaxApi().getOrders().then((orders) {
      setState(() { _orders = orders; });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) =>
      new Scaffold(
          appBar: new AppBar(title: new Text('Unofficial GDAX'), automaticallyImplyLeading: false),
          bottomNavigationBar: new BottomNav(currentIndex: 2),
          body: new ListView.builder(
            itemBuilder: (context, index) =>
            new GestureDetector(
              onTap: () => Navigator.of(context).push(new OrderPageRoute(_orders[index])),
              child: new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text('${_orders[index].createdAt}: ${_orders[index].productId} for ${_orders[index].executedValue}'))
              ),
            itemCount: _orders?.length ?? 0,
          )
  );
}