import 'package:flutter/material.dart';

class TradeBottomSheet extends StatefulWidget {
  TradeBottomSheet ({Key key}) : super(key: key);
  @override
  _TradeBottomSheetState createState() => new _TradeBottomSheetState();
}

enum TradeType { MARKET, LIMIT, STOP }
enum BuySell { BUY, SELL }

class _TradeBottomSheetState extends State<TradeBottomSheet> {
  TextEditingController _controller = new TextEditingController();
  TradeType _tradeType;
  BuySell _buySell;

  @override
  void initState() {
    super.initState();
    _tradeType = TradeType.MARKET;
    _buySell = BuySell.BUY;
  }

  VoidCallback _changeTradeType(tradeType) {
    return () {
      setState(() { _tradeType = tradeType; });
    };
  }

  VoidCallback _changeBuySell(buySell) {
    return () {
      setState(() { _buySell = buySell; });
    };
  }

  void _placeOrder() {

  }

  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    Color accentColor = Theme.of(context).accentColor;
    Color disabledColor = Theme.of(context).disabledColor;

    return new Container(
        color: Theme.of(context).primaryColor,
        child: new Padding(
          padding: const EdgeInsets.all(32.0),
          child: new Column(
            children: [
              new Row( children:
                  TradeType.values.map((v) => new FlatButton(
                      onPressed: _changeTradeType(v),
                      color: _tradeType == v ? accentColor : disabledColor,
                      child: new Text(v.toString().substring(v.toString().indexOf('.') + 1)),
                  )).toList()
              ),
              new Row( children:
                BuySell.values.map((v) => new RaisedButton(
                    onPressed: _changeBuySell(v),
                    color: _buySell == v ? accentColor : disabledColor,
                    child: new Text(v.toString().substring(v.toString().indexOf('.') + 1)),
                )).toList()
              ),
              new TextField(
                controller: _controller,
                keyboardType: TextInputType.number,
                decoration: new InputDecoration(hintText: 'USD'),
              ),
              new RaisedButton(
                  onPressed: _placeOrder,
                  color: primaryColor,
                  child: new Text('PLACE ORDER')
              ),
            ]
          )
        )
    );
  }
}
