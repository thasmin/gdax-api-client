import 'package:flutter/material.dart';
import 'package:gdaxclient/gdax_ticker.dart';

class PriceBar extends StatefulWidget {
  PriceBar({Key key}): super(key: key);
  @override
  _PriceBarState createState() => new _PriceBarState();
}

class _PriceBarState extends State<PriceBar> {
  Ticker _ticker;
  double _btcPrice;

  @override
  void initState() {
    super.initState();
    _ticker = new Ticker('BTC-USD', _handleTickerChange);
  }

  @override
  void dispose() {
    super.dispose();
    _ticker.close();
  }

  void _handleTickerChange(double value) {
    setState(() {
      _btcPrice = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      return new Container(
          color: Theme.of(context).accentColor,
          width: constraints.maxWidth,
          child: new Padding(
              padding: const EdgeInsets.all(16.0),
              child: new Text('BTC Price: $_btcPrice')
          )
      );
    });
  }
}
