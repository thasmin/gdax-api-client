import 'dart:async';
import 'dart:convert';

import 'package:crypto/crypto.dart' as crypto;
import 'package:http/http.dart' as http;

class GdaxHoldItem {
  // type can be order or transfer
  final String id; // uuid
  final String accountId; // uuid
  final DateTime createdAt;
  final DateTime updatedAt;
  final double amount;
  final String type;
  final String ref; // uuid, refers to order
  GdaxHoldItem(this.id, this.accountId, this.createdAt, this.updatedAt, this.amount, this.type, this.ref);
}

class GdaxLedgerItem {
  // type can be transfer, match, fee, rebate
  final int id;
  final DateTime createdAt;
  final double amount;
  final double balance;
  final String type;
  final Object details;
  GdaxLedgerItem(this.id, this.createdAt, this.amount, this.balance, this.type, this.details);
}

class GdaxAccount {
  final String id;
  final String currency;
  final double balance;
  final double available;
  final double hold;
  final String profileId;
  GdaxAccount(this.id, this.currency, this.balance, this.available, this.hold, this.profileId);
  String toString() { return "account with $balance of $currency"; }
}

class GdaxProduct {
  final String id;
  final String baseCurrency;
  final String quoteCurrency;
  final double baseMinSize;
  final double baseMaxSize;
  final double quoteIncrement;

  GdaxProduct(this.id, this.baseCurrency, this.quoteCurrency, this.baseMinSize, this.baseMaxSize, this.quoteIncrement);

  String toString() { return id; }
}

enum OrderStatus { All, Open, Pending, Active }

class GdaxOrder {
  final String id; // uuid
  final String productId;
  final String side;
  final String stp;
  final bool hasFunds;
  double _funds;
  double get funds => _funds;
  double _specifiedFunds;
  double get specifiedFunds => _specifiedFunds;
  final String type;
  final bool postOnly;
  final DateTime createdAt;
  final DateTime doneAt;
  final String doneAtReason;
  final double fillFees;
  final double filledSize;
  final double executedValue;
  final String status;
  final bool settled;

  GdaxOrder.withoutFunds(this.id, this.productId, this.side, this.stp, this.type, this.postOnly, this.createdAt, this.doneAt, this.doneAtReason, this.fillFees, this.filledSize, this.executedValue, this.status, this.settled) : hasFunds = false;
  GdaxOrder(this.id, this.productId, this.side, this.stp, this._funds, this._specifiedFunds, this.type, this.postOnly, this.createdAt, this.doneAt, this.doneAtReason, this.fillFees, this.filledSize, this.executedValue, this.status, this.settled) : hasFunds = true;

  String toString() { return id; }
}

class GdaxFill {
  final DateTime createdAt;
  final int tradeId;
  final String productId;
  final String orderId; // uuid
  final String userId;
  final String profileId; // uuid
  final String liquidity;
  final double price;
  final double size;
  final double fee;
  final String side;
  final bool settled;
  final double usdVolume;
  GdaxFill(this.createdAt, this.tradeId, this.productId, this.orderId, this.userId, this.profileId, this.liquidity, this.price, this.size, this.fee, this.side, this.settled, this.usdVolume);
  String toString() { return tradeId.toString() + " at " + createdAt.toString(); }
}

class GdaxApi {
  static const url = "https://api.gdax.com";

  static const key = "3e4ef280a2ce3b1d7871ef8a579c8ee0";
  static const passphrase = "7r7rbm9wu1c";
  static const secret = "dFhSckesOjHDwcc+pbYXjQk8TESwSxESlzh2usFeGcoPRfo6/ybNy+46JiktxlwrOMDZmvO2UzrIuI+dfMnBFg==";

  static List<GdaxProduct> _products;
  static String tempDir;

  Future<String> _get(String requestPath, { Object body }) async {
    var timestamp = new DateTime.now().toUtc().millisecondsSinceEpoch / 1000;
    var method = 'GET';
    var bodyJson = body == null ? '' : JSON.encode(body);
    var hmac = new crypto.Hmac(crypto.sha256, BASE64.decode(secret));
    var digest = hmac.convert(UTF8.encode(timestamp.toString() + method + requestPath + bodyJson));
    var sign = BASE64.encode(digest.bytes);

    var headers = {
      'CB-ACCESS-KEY': key,
      'CB-ACCESS-PASSPHRASE': passphrase,
      'CB-ACCESS-TIMESTAMP': timestamp,
      'CB-ACCESS-SIGN': sign
    };
    var response = await http.get(url + requestPath, headers: headers);
    if (response.statusCode < 200 || response.statusCode >= 300)
      throw(response.body);
    return response.body;
  }

  Future<List<GdaxProduct>> getProducts() async {
    // check cache
    if (_products != null)
      return _products;

    // check filesystem
    /*
    tempDir ??= (await path.getTemporaryDirectory()).path;
    var productsFile = new File(tempDir + "/products.json");
    if (productsFile.existsSync()) {
      _products = JSON.decode(productsFile.readAsStringSync());
      return _products;
    }
    */

    // get from api
    try {
      var response = JSON.decode(await _get('/products'));
      return response.map((obj) =>
        new GdaxProduct(
            obj['id'],
            obj['base_currency'],
            obj['quote_currency'],
            double.parse(obj['base_min_size']),
            double.parse(obj['base_max_size']),
            double.parse(obj['quote_increment'])
        )
      ).toList();
    } catch (e) {
      print(JSON.decode(e)['message']);
      return null;
    }
  }

  Future<List<GdaxAccount>> getAccounts() async {
    try {
      var response = JSON.decode(await _get('/accounts'));
      return response.map((obj) =>
        new GdaxAccount(
            obj['id'],
            obj['currency'],
            double.parse(obj['balance']),
            double.parse(obj['available']),
            double.parse(obj['hold']),
            obj['profile_id']
        )
      ).toList();
    } catch (e) {
      print(JSON.decode(e)['message']);
      return null;
    }
  }

  Future<List<GdaxLedgerItem>> getAccountHistory(String accountId) async {
    try {
      var response = JSON.decode(await _get('/accounts/$accountId/ledger'));
      return response.map((obj) =>
        new GdaxLedgerItem(
          obj['id'],
          DateTime.parse(obj['created_at']),
          double.parse(obj['amount']),
          double.parse(obj['balance']),
          obj['type'],
          obj['details'],
        )
      ).toList();
    } catch (e) {
      print(JSON.decode(e)['message']);
      return null;
    }
  }

  Future<List<GdaxHoldItem>> getAccountHolds(String accountId) async {
    try {
      var response = JSON.decode(await _get('/accounts/$accountId/holds'));
      return response.map((obj) =>
        new GdaxHoldItem(
          obj['id'],
          obj['accountId'],
          DateTime.parse(obj['createdAt']),
          DateTime.parse(obj['updatedAt']),
          double.parse(obj['amount']),
          obj['type'],
          obj['ref'],
        )
      ).toList();
    } catch (e) {
      print(JSON.decode(e)['message']);
      return null;
    }
  }

  Future<List<GdaxOrder>> getOrders({OrderStatus status = OrderStatus.All}) async {
    try {
      var statusText = "all";
      switch (status) {
        case OrderStatus.All:
          statusText = "all";
          break;
        case OrderStatus.Active:
          statusText = "active";
          break;
        case OrderStatus.Open:
          statusText = "open";
          break;
        case OrderStatus.Pending:
          statusText = "pending";
          break;
      }
      var response = JSON.decode(await _get('/orders?status=' + statusText));
      return response.map((obj) =>
        obj['funds'] != null ?
          new GdaxOrder(
              obj['id'],
              obj['product_id'],
              obj['side'],
              obj['stp'],
              double.parse(obj['funds']),
              double.parse(obj['specified_funds']),
              obj['type'],
              obj['post_only'],
              DateTime.parse(obj['created_at']),
              DateTime.parse(obj['done_at']),
              obj['done_reason'],
              double.parse(obj['fill_fees']),
              double.parse(obj['filled_size']),
              double.parse(obj['executed_value']),
              obj['status'],
              obj['settled']
        ) : new GdaxOrder.withoutFunds(
              obj['id'],
              obj['product_id'],
              obj['side'],
              obj['stp'],
              obj['type'],
              obj['post_only'],
              DateTime.parse(obj['created_at']),
              DateTime.parse(obj['done_at']),
              obj['done_reason'],
              double.parse(obj['fill_fees']),
              double.parse(obj['filled_size']),
              double.parse(obj['executed_value']),
              obj['status'],
              obj['settled']
        )
      ).toList();
    } catch (e) {
      print(JSON.decode(e)['message']);
      return null;
    }
  }

  Future<List<GdaxFill>> getFills() async {
    try {
      var response = JSON.decode(await _get('/fills'));
      return response.map((obj) =>
        new GdaxFill(
          DateTime.parse(obj['created_at']),
          obj['trade_id'],
          obj['product_id'],
          obj['order_id'],
          obj['user_id'],
          obj['profile_id'],
          obj['liquidity'],
          double.parse(obj['price']),
          double.parse(obj['size']),
          double.parse(obj['fee']),
          obj['side'],
          obj['settled'],
          double.parse(obj['usd_volume']),
        )
      ).toList();
    } catch (e) {
      print(JSON.decode(e)['message']);
      return null;
    }
  }

}

void main() {
  GdaxApi api = new GdaxApi();
  api.getAccounts().then((accounts) {
    print(accounts?.length.toString() + " accounts returned");
    accounts?.forEach((acc) {
      api.getAccountHistory(acc.id).then((history) {
        if (history == null) {
          print('error getting history for ' + acc.toString());
          return;
        }
        print(acc.toString() + ' has ${history.length} transactions');
      });
    });
    accounts?.forEach((acc) {
      api.getAccountHolds(acc.id).then((history) {
        if (history == null) {
          print('error getting holds for ' + acc.toString());
          return;
        }
        print(acc.toString() + ' has ${history.length} holds');
      });
    });
  });

  api.getOrders().then((orders) {
    print(orders?.length.toString() + " orders returned");
    orders?.forEach((order) {
      print("order to " + order.side + " " + order.filledSize.toString() + " " + order.productId + " for " + order.executedValue.toString() + " at " + order.createdAt.toString());
    });
  });

  api.getFills().then((fills) {
    print(fills?.length.toString() + ' fills returned');
    fills?.forEach((fill) {
      print('fill on ' + fill.tradeId.toString() + ' at ' + fill.createdAt.toString() + ' for ' + fill.size.toString() + ' ' + fill.productId);
    });
  });
}