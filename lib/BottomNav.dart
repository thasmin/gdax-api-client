import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class BottomNav extends StatelessWidget {
  final int currentIndex;

  BottomNav({@required this.currentIndex});

  @override
  Widget build(BuildContext context) =>
      new BottomNavigationBar(
        items: [
          new BottomNavigationBarItem(icon: const Icon(Icons.poll), title: new Text('Trade')),
          new BottomNavigationBarItem(icon: const Icon(Icons.business_center), title: new Text('Account')),
          new BottomNavigationBarItem(icon: const Icon(Icons.settings_ethernet), title: new Text('Orders')),
          new BottomNavigationBarItem(icon: const Icon(Icons.done), title: new Text('Fills')),
        ],
        currentIndex: this.currentIndex,
        onTap: (int value) {
          if (value == currentIndex)
            return;
          if (value == 0) Navigator.of(context).pushNamed("/");
          else if (value == 1) Navigator.of(context).pushNamed("/accounts");
          else if (value == 2) Navigator.of(context).pushNamed("/orders");
          else if (value == 3) Navigator.of(context).pushNamed("/fills");
        },
      );
}
