import 'dart:async';
import 'dart:convert';
import 'dart:io';

class Ticker {
  WebSocket _ws;

  final String product;
  final Function onTickerChange;

  void _handleMessage(event) {
    var response = JSON.decode(event);
    if (response['type'] == 'ticker')
        Function.apply(onTickerChange, [double.parse(response['price'])]);
  }

  Ticker(this.product, this.onTickerChange) {
    WebSocket.connect('wss://ws-feed.gdax.com').then((websocket) {
      _ws = websocket;

      var subscribeMsg = {
        'type': 'subscribe',
        'product_ids': [ '$product' ],
        'channels': [ 'ticker' ]
      };

      websocket.add(JSON.encode(subscribeMsg));
      websocket.listen(_handleMessage);
    });
  }

  void close() {
    _ws.close();
  }

}

bool _gotticker = false;
void handleTicker(double value) {
  _gotticker = true;
  print('ticker is at $value');
}

void main() {
  var ticker = new Ticker('BTC-USD', handleTicker);
  var start = new DateTime.now();
  new Timer.periodic(new Duration(seconds: 1), (timer) {
    if (_gotticker) {
      print('success');
      ticker.close();
      timer.cancel();
    }
    if (new DateTime.now().subtract(new Duration(seconds: 5)).isAfter(start)) {
      print('failure');
      ticker.close();
      timer.cancel();
    }
  });
}
