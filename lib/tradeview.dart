import 'package:flutter/material.dart';
import 'package:gdaxclient/BottomNav.dart';
import 'package:gdaxclient/gdax_api.dart';
import 'package:gdaxclient/gdax_orderbook.dart';
import 'package:gdaxclient/tradebottomsheet.dart';

class TradeView extends StatefulWidget {
  TradeView({Key key}) : super(key: key);
  @override
  _TradeViewState createState() => new _TradeViewState();
}

class _TradeViewState extends State<TradeView> {
  static OrderBook _orderbook;
  String _product;
  List<DropdownMenuItem<String>> _products;

  @override
  void initState() {
    super.initState();
    //_orderbook = new OrderBook('BTC-USD', _onReceivedBook, _onUpdate);
    new GdaxApi().getProducts().then((products) {
      setState(() {
        _products = products.map((p) => new DropdownMenuItem(child: new Text(p.id), value: p.id)).toList();
      });
      if (_product == null)
        _changeProduct('BTC-USD');
    });
  }

  @override
  void dispose() {
    super.dispose();
    _orderbook.close();
  }

  void _changeProduct(newProduct) {
    setState(() {
      _product = newProduct;
    });
    if (_orderbook?.product != _product) {
      _orderbook?.close();
      _orderbook = new OrderBook(_product, _onReceivedBook, _onUpdate);
    }
  }

  void _onReceivedBook() {
    setState(() { });
  }

  void _onUpdate(updates) {
    setState(() { });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Unofficial GDAX'), automaticallyImplyLeading: false),
      bottomNavigationBar: new BottomNav(currentIndex: 0),
      body: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            new DropdownButton<String>(
              value: _product,
              items: _products ?? [],
              onChanged: _changeProduct,
            ),
            new Expanded(
                child: new ListView.builder(
                  itemBuilder: (BuildContext context, int index) =>
                  new Text(
                    _orderbook.asks.keys.elementAt(index).toStringAsFixed(2) +
                    ": " +
                    _orderbook.asks.values.elementAt(index).toString()
                  ),
                  itemCount: _orderbook?.asks?.length ?? 0,
                  reverse: true,
                )
            ),
            new LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) =>
                new Container(
                  width: constraints.maxWidth,
                  decoration: new BoxDecoration(border: new Border(
                    top: new BorderSide(width: 2.0, color: Colors.red),
                    bottom: new BorderSide(width: 2.0, color: Colors.red),
                  )),
                  child: new Text('SPREAD: ${_orderbook?.spread?.toStringAsFixed(2) ?? '0.00'}'),
                )
            ),
            new Expanded(
                child: new ListView.builder(
                  itemBuilder: (BuildContext context, int index) =>
                  new Text(
                    _orderbook.bids.keys.elementAt(index).toStringAsFixed(2) +
                    ": " +
                    _orderbook.bids.values.elementAt(index).toString()),
                  itemCount: _orderbook?.bids?.length ?? 0,
                )
            ),
          ]
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(context: context, builder: (context) => new TradeBottomSheet());
        },
        tooltip: 'Place Order',
        child: new Icon(Icons.add),
      ),
    );
  }
}
