import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

class OrderBook {
  WebSocket _ws;

  final String product;
  final Function onReceivedBook;
  final Function onUpdate;

  final asks = new SplayTreeMap<double, double>();
  final bids = new SplayTreeMap<double, double>((key1, key2) => -Comparable.compare(key1, key2));

  double get spread => this.bids.length == 0 && this.asks.length == 0 ? 0.0 : this.asks.keys.first - this.bids.keys.first;

  Timer _updateTimer;
  var _updates = {};

  void _handleMessage(event) {
    var response = JSON.decode(event);
    switch (response['type']) {
      case 'snapshot':
        this.bids.clear();
        this.asks.clear();
        response['bids'].forEach((line) { this.bids[double.parse(line[0])] = double.parse(line[1]); });
        response['asks'].forEach((line) { this.asks[double.parse(line[0])] = double.parse(line[1]); });
        if (onReceivedBook != null)
          Function.apply(onReceivedBook, []);

        _updateTimer?.cancel();
        _updateTimer = new Timer.periodic(new Duration(seconds: 1), (timer) {
          // race condition
          if (onUpdate != null)
            Function.apply(onUpdate, [this._updates]);
          _updates['buys'].clear();
          _updates['sells'].clear();
        });
        break;
      case 'l2update':
        response['changes'].forEach((line) {
          var side = line[0];
          var price = double.parse(line[1]);
          var amount = double.parse(line[2]);
          var target = side == 'buy' ? this.bids : this.asks;
          if (amount == 0)
            target.remove(price);
          else
            target[price] = amount;

          this._updates[side + 's'][price] = amount;
        });
        break;
    }
  }

  OrderBook(this.product, this.onReceivedBook, this.onUpdate) {
    _updates = { 'buys': {}, 'sells': {} };

    WebSocket.connect('wss://ws-feed.gdax.com').then((websocket) {
      _ws = websocket;

      var subscribeMsg = {
        'type': 'subscribe',
        'product_ids': [ '$product' ],
        'channels': [ 'level2' ]
      };

      websocket.add(JSON.encode(subscribeMsg));
      websocket.listen(_handleMessage);
    });
  }

  void close() {
    _ws.close();
    _updateTimer?.cancel();
  }

}

OrderBook _orderbook;
bool _receivedBook = false;
bool _receivedUpdate = false;
int _prints = 5;
var originalBids = {};
var originalAsks = {};

void handleOrderBook() {
  _receivedBook = true;
  print('asks');
  _orderbook.asks.keys.take(_prints).forEach((price) {
    var amount = _orderbook.asks[price];
    originalAsks[price] = amount;
    print('$price: $amount');
  });

  print('bids');
  _orderbook.bids.keys.take(_prints).forEach((price) {
    var amount = _orderbook.bids[price];
    originalBids[price] = amount;
    print('$price: $amount');
  });

  _startChangeTimer();
}

void handleUpdate(updates) {
  if (updates['buys'].length == 0 && updates['sells'].length == 0) {
    print('no buy or sell updates');
    return;
  }
  print('buy updates: ${updates['buys'].length}');
  print('sell updates: ${updates['sells'].length}');
}

void main() {
  _orderbook = new OrderBook('BTC-USD', handleOrderBook, handleUpdate);
  new Timer(new Duration(seconds: 2), () {
    if (!_receivedBook) {
      print('never received book');
      _orderbook.close();
    }
  });
}

void _startChangeTimer() {
  new Timer(new Duration(seconds: 3), () {
    if (!_receivedBook) {
      print('never received book');
      return;
    } else {
      if (!_receivedUpdate)
        print('no updates');

      bool _allSame = true;
      originalBids.forEach((price, amount) {
        if (!_orderbook.bids.keys.take(originalBids.length).contains(price))
          _allSame = false;
        if (_orderbook.bids[price] != amount)
          _allSame = false;
      });
      originalAsks.forEach((price, amount) {
        if (!_orderbook.asks.keys.take(originalAsks.length).contains(price))
          _allSame = false;
        if (_orderbook.asks[price] != amount)
          _allSame = false;
      });
      if (_allSame)
        print('no changes to book');
      else
        print('ok');
    }
    _orderbook.close();
  });
}
