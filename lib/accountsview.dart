import 'package:flutter/material.dart';
import 'package:gdaxclient/BottomNav.dart';
import 'package:gdaxclient/gdax_api.dart';
import 'package:intl/intl.dart';

class AccountHoldsView extends StatefulWidget {
  final GdaxAccount account;
  AccountHoldsView(this.account, {Key key}) : super(key: key);
  @override
  _AccountHoldsViewState createState() => new _AccountHoldsViewState();
}

class _AccountHoldsViewState extends State<AccountHoldsView> {
  List<GdaxHoldItem> _holdItems;

  @override
  void initState() {
    super.initState();
    new GdaxApi().getAccountHolds(widget.account.id).then((holds) {
      setState(() { _holdItems = holds; });
    });
  }

  @override
  Widget build(BuildContext context) {
    var account = widget.account;
    var theme = Theme.of(context);
    return _holdItems != null && _holdItems.length == 0 ?
      new Padding(padding: const EdgeInsets.all(16.0), child: new Text("No holds in this account")) :
      new ListView.builder(
          itemCount: _holdItems?.length ?? 0,
          itemBuilder: (context, index) => new Card(
            child: new Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Table(
                columnWidths: {0: new FlexColumnWidth(1.0), 1: new FlexColumnWidth(1.0)},
                children: [
                  new TableRow(children: [
                    new Text('Type', style: Theme.of(context).primaryTextTheme.subhead),
                    new Text(_holdItems[index].type)
                  ]),
                  new TableRow(children: [
                    new Text('Time', style: theme.primaryTextTheme.subhead),
                    new Text(new DateFormat.yMMMd().format(_holdItems[index].createdAt))
                  ]),
                  new TableRow(children: [
                    new Text('Amount (${account.currency})', style: theme.primaryTextTheme.subhead),
                    new Text(_holdItems[index].amount.toString())
                  ]),
                ]
              )
            )
          )
      );
  }
}

class AccountHistoryView extends StatefulWidget {
  final GdaxAccount account;
  AccountHistoryView(this.account, {Key key}) : super(key: key);
  @override
  _AccountHistoryViewState createState() => new _AccountHistoryViewState();
}

class _AccountHistoryViewState extends State<AccountHistoryView> {
  List<GdaxLedgerItem> _ledgerItems;

  @override
  void initState() {
    super.initState();
    new GdaxApi().getAccountHistory(widget.account.id).then((history) {
      setState(() { _ledgerItems = history; });
    });
  }

  @override
  Widget build(BuildContext context) {
    var account = widget.account;
    var theme = Theme.of(context);
    return _ledgerItems != null && _ledgerItems.length == 0 ?
      new Padding(padding: const EdgeInsets.all(16.0), child: new Text("No history for this account")) :
      new ListView.builder(
          itemCount: _ledgerItems?.length ?? 0,
          itemBuilder: (context, index) =>
          new Card(
              child: new Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Table(
                      columnWidths: {
                        0: new FlexColumnWidth(1.0),
                        1: new FlexColumnWidth(1.0)
                      },
                      children: [
                        new TableRow(children: [
                          new Text('Type', style: theme.primaryTextTheme.subhead),
                          new Text(_ledgerItems[index].type)
                        ]),
                        new TableRow(children: [
                          new Text('Time', style: theme.primaryTextTheme.subhead),
                          new Text(new DateFormat.yMMMd().format(_ledgerItems[index].createdAt))
                        ]),
                        new TableRow(children: [
                          new Text('Amount (${account.currency})', style: theme.primaryTextTheme.subhead),
                          new Text(_ledgerItems[index].amount.toString())
                        ]),
                        new TableRow(children: [
                          new Text('Balance (${account.currency})', style: theme.primaryTextTheme.subhead),
                          new Text(_ledgerItems[index].balance.toString())
                        ]),
                      ]
                  )
              )
          )
      );
  }
}

class AccountView extends StatefulWidget {
  final GdaxAccount account;

  AccountView(this.account, {Key key}) : super(key: key);

  @override
  _AccountViewState createState() => new _AccountViewState();
}

class _AccountViewState extends State<AccountView> {
  AccountsPages _page = AccountsPages.History;
  AccountHistoryView _historyView;
  AccountHoldsView _holdsView;

  void _viewHistory() {
    setState(() {
      _page = AccountsPages.History;
    });
  }

  void _viewHolds() {
    setState(() {
      _page = AccountsPages.Holds;
    });
  }

  @override
  void initState() {
    super.initState();
    _historyView = new AccountHistoryView(widget.account);
    _holdsView = new AccountHoldsView(widget.account);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
      title: new Text(widget.account.currency + ' History')),
      body: new Padding(
        padding: const EdgeInsets.all(8.0),
        child: new Column(children: [
          new Row(children: [
            new Expanded(child: new RaisedButton(onPressed: _viewHistory, child: new Text('History'),)),
            new Expanded(child: new RaisedButton(onPressed: _viewHolds, child: new Text('Holds'),)),
          ]),
          new Expanded(child: _page == AccountsPages.History ? _historyView : _holdsView),
        ])
      )
    );
  }
}

class AccountPageRoute extends MaterialPageRoute {
  AccountPageRoute(GdaxAccount account) : super(builder: (ctx) => new AccountView(account));
}

enum AccountsPages { History, Holds }

class AccountsView extends StatefulWidget {
  AccountsView({Key key}) : super(key: key);
  @override
  _AccountsViewState createState() => new _AccountsViewState();
}

class _AccountsViewState extends State<AccountsView> {
  List<GdaxAccount> _accounts = new List<GdaxAccount>();

  @override
  void initState() {
    new GdaxApi().getAccounts().then((accounts) {
      setState(() { _accounts = accounts; });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) =>
      new Scaffold(
          appBar: new AppBar(title: new Text('Unofficial GDAX'), automaticallyImplyLeading: false),
          bottomNavigationBar: new BottomNav(currentIndex: 1),
          body: new ListView.builder(
            itemBuilder: (context, index) =>
            new GestureDetector(
              onTap: () => Navigator.of(context).push(new AccountPageRoute(_accounts[index])),
              child: new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text('${_accounts[index].currency}: ${_accounts[index].balance}'))
              ),
            itemCount: _accounts?.length ?? 0,
          )
  );
}
