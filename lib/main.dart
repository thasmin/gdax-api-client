import 'package:flutter/material.dart';
import 'package:gdaxclient/FillsView.dart';
import 'package:gdaxclient/OrdersView.dart';
import 'package:gdaxclient/accountsview.dart';
import 'package:gdaxclient/tradeview.dart';

import 'gdax_api.dart';

GdaxApi _gdaxApi = new GdaxApi();

void main() => runApp(new App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'GDAX App',
      theme: new ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.orange,
        primaryColor: Colors.orange,
        accentColor: Colors.deepPurpleAccent,
      ),
      routes: {
        "/": (ctx) => new TradeView(),
        "/trade": (ctx) => new TradeView(),
        "/accounts": (ctx) => new AccountsView(),
        "/orders": (ctx) => new OrdersView(),
        "/fills": (ctx) => new FillsView(),
      },
    );
  }
}

