import 'package:flutter/material.dart';
import 'package:gdaxclient/BottomNav.dart';
import 'package:gdaxclient/gdax_api.dart';


class FillPageRoute extends MaterialPageRoute {
  FillPageRoute(GdaxFill fill) : super(builder: (ctx) => new FillView(fill));
}

class DataTableRow extends TableRow {
  DataTableRow(ThemeData theme, String heading, String value) : super(
      children: [
        new Padding( padding: const EdgeInsets.all(4.0), child: new Text(heading, style: new TextStyle(color: theme.primaryColor))),
        new Padding( padding: const EdgeInsets.all(4.0), child: new Text(value)),
      ]
  );
}

class FillView extends StatelessWidget {
  final GdaxFill fill;
  FillView(this.fill);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return new Scaffold(
        appBar: new AppBar(
            title: new Text('Fill from ' + fill.createdAt.toString())),
        body: new Padding(
            padding: const EdgeInsets.all(8.0),
            child: new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Table(
                  columnWidths: { 0: const IntrinsicColumnWidth() },
                  children: [
                    new DataTableRow(theme, 'Created At', fill.createdAt.toString()),
                    new DataTableRow(theme, 'Trade', fill.tradeId.toString()),
                    new DataTableRow(theme, 'Product', fill.productId),
                    new DataTableRow(theme, 'Order', fill.orderId),
                    new DataTableRow(theme, 'User', fill.userId),
                    new DataTableRow(theme, 'Profile', fill.profileId),
                    new DataTableRow(theme, 'Liquidity', fill.liquidity),
                    new DataTableRow(theme, 'Price', fill.price.toString()),
                    new DataTableRow(theme, 'Size', fill.size.toString()),
                    new DataTableRow(theme, 'Fee', fill.fee.toString()),
                    new DataTableRow(theme, 'Side', fill.side),
                    new DataTableRow(theme, 'Settled', fill.settled ? 'Yes' : 'No'),
                    new DataTableRow(theme, 'USD Volume', fill.usdVolume.toString()),
                  ],
                )
            )
        )
    );
  }
}

class FillsView extends StatefulWidget {
  FillsView({Key key}) : super(key: key);
  @override
  _FillsViewState createState() => new _FillsViewState();
}

class _FillsViewState extends State<FillsView> {
  List<GdaxFill> _fills = new List<GdaxFill>();

  @override
  void initState() {
    new GdaxApi().getFills().then((fills) {
      setState(() { _fills = fills; });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) =>
      new Scaffold(
          appBar: new AppBar(title: new Text('Unofficial GDAX'), automaticallyImplyLeading: false),
          bottomNavigationBar: new BottomNav(currentIndex: 3),
          body: new ListView.builder(
            itemBuilder: (context, index) =>
            new GestureDetector(
                onTap: () => Navigator.of(context).push(new FillPageRoute(_fills[index])),
                child: new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Text('${_fills[index].createdAt}: ${_fills[index].size} ${_fills[index].productId}'))
            ),
            itemCount: _fills?.length ?? 0,
          )
      );
}
